using System;

namespace Tronco.Core
{

    public class MeasurementLogEntry : LogEntry
    {
        public TimeSpan ElapsedTime { get; private set; }

        public MeasurementLogEntry(TimeSpan timeSpan, string message = "")
            :base(ELogLevel.Debug, message, new object[0])
        {
            ElapsedTime = timeSpan;
        }

        override public object GetInternalData()
        {
            return new
            {
                elapsedtime = ElapsedTime.ToString(),
                level = LogLevel,
                timestamp = Timestamp,
                message = Message,
                stacktrace = StackTraceSnapshot,
                stackframe = StackFrameSnapshot,
                newline = Environment.NewLine,
                time_now = DateTime.Now,
                method = MethodName,
                filename = FileName ?? string.Empty,
                linenumber = LineNumber > 0 ? LineNumber.ToString() : string.Empty
            };
        }

    }

}
