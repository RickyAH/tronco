namespace Tronco.Core
{
    /// <summary>
    ///     Log levels. Used to filter log entries.
    /// </summary>
    public enum ELogLevel 
    {
        Fatal = 4,
        Error = 3,
        Warning = 2,
        Info = 1,   
        Debug = 0
    }
}
