using System.Collections.Generic;
using System;

namespace Tronco.Core
{
    /// <summary>
    ///     Defines the signature for events generated in a ILoggerSet instance.
    /// </summary>
    public delegate void LogEventHandler(ILogger logName);

    /// <summary>
    ///     Interface for storing a set of ILogger instances. 
    ///     Instances can be retrieved using the ILogger.Name property value
    /// </summary>
    public interface ILoggerSet : IEnumerable<ILogger>
    {
        /// <summary>
        ///     Gets a logger by name. Returns null if a logger with that name was not found.
        /// </summary>
        /// <param name="name">Name.</param>
        ILogger this[string name] {get;}

        /// <summary>
        ///     Adds a logger instance to the set.
        ///     Loggers are referenced by name, so you can't have two loggers with the same name in the
        ///     same set.
        /// </summary>
        /// <returns>
        ///     true if the logger was added to the set, otherwise false (due to another log with the 
        ///     same name already exists in the set.
        /// </returns>
        bool Add(ILogger logger);

        /// <summary>
        ///     Removes a logger instance from the set
        /// </summary>
        /// <returns>
        ///     true if the logger was removed fromthe collection,
        ///     or false if the logger did not exist in this collection
        /// </returns>
        bool Remove(ILogger logger);

        /// <summary>
        ///     Raised when a new log is added to the set
        /// </summary>
        event LogEventHandler LogAdded;

        /// <summary>
        ///     Raised when a log is removed from the set
        /// </summary>
        event LogEventHandler LogRemoved;
    }
}