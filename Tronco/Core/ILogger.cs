using System;

namespace Tronco.Core
{
    /// <summary>
    ///     Delegate signature for ILogger.LogEntryCreated events
    /// </summary>
    public delegate void LogEntryCreatedHandler(LogEntry logEntry);


    /// <summary>
    ///     ILogger the primary interface used by the end user of the log system after
    ///     such system has been initialized and a concrete instance of a Logger class
    ///     is created.
    /// </summary>
    public interface ILogger
    {

        /// <summary>
        ///     Notifyes whenever a log entry has been created. 
        ///     Logs targets added to this Logger register to this event
        ///     So they can process the log entries
        /// </summary>
        event LogEntryCreatedHandler LogEntryCreated;

        /// <summary>
        ///     Allows enabling / disabling the current logger
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        bool IsEnabled {get; set;}

        /// <summary>
        ///     Logger's name. It is treated as an unique id by a LogManager.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }

        /// <summary>
        ///     Minimum Log level needed for a LogEntry to be processed.
        /// </summary>
        ELogLevel MinimumLevel {get;set;}
        
        void Fatal(Func<string> deferredLog);        

        void Fatal(string message, params object[] objs);

        void Error(Func<string> deferredLog);

        void Error(string message, params object[] objs);

        void Warn(Func<string> deferredLog);

        void Warn(string message, params object[] objs);

        void Info(Func<string> deferredLog);

        void Info(string message, params object[] objs);

        void Debug(Func<string> deferredLog);

        void Debug(string message, params object[] objs);
       
    }
}