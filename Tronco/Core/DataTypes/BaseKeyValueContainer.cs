using System.Collections;
using System.Collections.Generic;

namespace Tronco.Core.DataTypes
{
    public class BaseKeyValueContainer<TId, TContainment> : IEnumerable<TContainment>
        where TContainment : class
    {
        protected Dictionary<TId, TContainment> ContainmentList { get; private set; }

        public BaseKeyValueContainer()
        {
            {
                ContainmentList = new Dictionary<TId, TContainment>();
            }
        }
       
        public bool Register(TId id, TContainment containment)
        {
            if (!ContainmentList.ContainsKey(id))
            {
                ContainmentList.Add(id, containment);
                return true;
            }
            return false;
        }

        public TContainment Remove(TId id)
        {
            TContainment containment = Retrieve(id);
            if (containment != null)
            {
                ContainmentList.Remove(id); 
            }

            return containment;
        }

        public TContainment Retrieve(TId id)
        {
            TContainment containment;
            if (ContainmentList.TryGetValue(id, out containment))
            {
                return containment;
            }

            return default(TContainment);
        }

        public IEnumerator<TContainment> GetEnumerator()
        {
            return ContainmentList.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}