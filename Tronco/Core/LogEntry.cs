using System;
using System.Diagnostics;

namespace Tronco.Core
{

    /// <summary>
    ///     Defines a LogEntry containing all the data 
    /// </summary>
    public class LogEntry
    {
        /// <summary>
        ///     Log level for this entry
        /// </summary>
        public ELogLevel LogLevel { get; private set; }

        /// <summary>
        ///     Timestamp when this entry was originated
        /// </summary>
        public DateTime Timestamp { get; private set; }

        /// <summary>
        ///     StackTtrace snapshot in the moment this entry was originated
        /// </summary>
        public StackTrace StackTraceSnapshot { get; internal set; }

        /// <summary>
        ///     StackFrame where this entry was originated.
        /// </summary>
        public StackFrame StackFrameSnapshot 
        { 
            get { return StackTraceSnapshot.GetFrame(0); } 
        }

        /// <summary>
        ///     Name of the method where this log entry originated.
        /// </summary>
        public string MethodName
        {
            get { return StackFrameSnapshot.GetMethod().ToString(); }
        }

        /// <summary>
        ///     Filename where this log entry originated. (could be unavailable)
        /// </summary>
        public string FileName
        {
            get { return StackFrameSnapshot.GetFileName(); }
        }

        /// <summary>
        ///     LineNumber inside the filename where this log entry originated. (could be unavailable)
        /// </summary>
        public int LineNumber
        {
            get { return StackFrameSnapshot.GetFileLineNumber(); }
        }

        /// <summary>
        ///     Returns the message string for this log entry.
        /// </summary>
        /// <remarks>
        ///     The message is generated using a lazy evaluation.
        /// </remarks>
        public string Message 
        {
            get 
            {
                if (string.IsNullOrEmpty(_messageCached))
                {
                    if (_createMsgDelegate == null)
                    {
                        _messageCached = CreateNewLineSeparatedString(
                            LogEntryErrorMessages.ErrorStrNullDelegate, 
                            LogEntryErrorMessages.AdviseConsultStackTrace,
                            StackTraceSnapshot.ToString());
                    }
                    else
                    {
                        _messageCached = _createMsgDelegate();
                    }

                }

                return _messageCached;
            }
        }
            
        /// <summary>
        ///     Returns a representation of the internal state as an anonymous object
        ///     so it is accesible by a string formatter that maps fields in 
        ///     an objects by name (see FormatStringKyewordMatcher.cs)
        ///     This will allow creating a log format string which defines how the data 
        ///     in a ILogEntry is converted to a string (see ILogEntryFormatter)
        /// </summary>
        /// <remarks>
        ///     By convention, all the fields are named using lowercase.
        ///     This method can be overwritten in derived classes in order to provide
        ///     more information about a log entry. See MeasurementLogEntry for an example
        ///     of a new ILogEntry implementation that adds information about the time that
        ///     an operation took to complete.
        /// </remarks>
        virtual public object GetInternalData()
        {
            return new
            {
                level = LogLevel,
                timestamp = Timestamp,
                message = Message,
                stacktrace = StackTraceSnapshot,
                stackframe = StackFrameSnapshot,
                newline = Environment.NewLine,
                time_now = DateTime.Now,
                method = MethodName,
                filename = FileName ?? string.Empty,
                linenumber = LineNumber > 0 ? LineNumber.ToString() : string.Empty
            };
        }
            

        #region Initialization

        /// <summary>
        ///     Ctor. 
        /// </summary>
        /// <param name="level">
        ///     Level of the log entry
        /// </param>
        public LogEntry(ELogLevel level)
        {
            LogLevel = level;
            Timestamp = DateTime.Now;
            StackTraceSnapshot = new StackTrace(true);
        }

        /// <summary>
        ///     Ctor.
        /// </summary>
        /// <param name="level">
        ///     Level of the log entry
        /// </param>
        /// <param name="formatMsgStringMsg">
        ///     Message string. Can be formatted
        /// </param>
        /// <param name="objs">
        ///     Optional parameters for the formatted string
        /// </param>
        public LogEntry(ELogLevel level, string formatMsgStringMsg, params object[] objs)
            : this(level)
        {
            _messageCached = SafeFormatMessageString(formatMsgStringMsg, objs);
        }

        /// <summary>
        ///     Ctor.
        /// </summary>
        /// <param name="level">
        ///     Level of the log entry
        /// </param>
        /// <param name="createMsgDelegate">
        ///     Delegate that computes the message string when it is invoked.
        /// </param>
        public LogEntry(ELogLevel level, Func<string> createMsgDelegate)
            : this(level)
        {
            _createMsgDelegate = createMsgDelegate;
        }

        #endregion Initialization

        #region Internal data

        /// <summary>
        ///     Reference to the Logger that crated this LogEntry
        /// </summary>
        /// <value>The logger reference.</value>
        internal ILogger LoggerRef { get; set; }

        private readonly Func<string> _createMsgDelegate;
        private string _messageCached;

        #endregion

        #region Helpers

        /// <summary>
        ///     Creates a formatted string in a safe way: if any error is found returns an error message.
        /// </summary>
        protected string SafeFormatMessageString(string formatMsgStringMsg, object[] objs)
        {
            if (formatMsgStringMsg.Trim() == string.Empty)
            {
                return CreateNewLineSeparatedString(
                    LogEntryErrorMessages.MessageStrEmptyMessage, 
                    LogEntryErrorMessages.AdviseConsultStackTrace,
                    StackTraceSnapshot.ToString());
            }

            try
            {
                return string.Format(formatMsgStringMsg, objs);
            }
            catch (ArgumentNullException)
            {
                return CreateNewLineSeparatedString(
                    LogEntryErrorMessages.MessageStrNull,
                    LogEntryErrorMessages.AdviseConsultStackTrace,
                    StackTraceSnapshot.ToString());
            }
            catch (FormatException ex)
            {
                return CreateNewLineSeparatedString(
                    string.Format(LogEntryErrorMessages.BadFormattedMessage, objs.Length, formatMsgStringMsg, ex),
                    LogEntryErrorMessages.AdviseConsultStackTrace,
                    StackTraceSnapshot.ToString());
            }
        }
            
        /// <summary>
        ///     Creates an string separated by new lines given a set of phrases
        /// </summary>
        string CreateNewLineSeparatedString(params string[] phrases)
        {
            return String.Join(Environment.NewLine, phrases);
        }

        #endregion

        /// <summary>
        ///     Holds some human-readable error messages
        /// </summary>
        internal static class LogEntryErrorMessages
        {

            public static readonly string AdviseConsultStackTrace = "See stacktrace to see where the log originated";

            public static readonly string MessageStrNull = "A LogEntry requires a message but got a null string instead.";

            public static readonly string MessageStrEmptyMessage = "A LogEntry requires a message but got an empty string instead.";

            public static readonly string BadFormattedMessage
            = "A LogEntry allows a formatted string, but this one receiving {0} arguments is bad formed: \"{1}\" and thrown this exception:"
                + Environment.NewLine
                + "{2}";

            public static readonly string ErrorStrNullDelegate = "A LogEntry required a Func<string> delegate but got a null reference instead.";

        }
    }
}

