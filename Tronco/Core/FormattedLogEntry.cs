namespace Tronco.Core
{
    /// <summary>
    ///     Aggregate structure to store a LogEntry and its
    ///     string representation after being processed by
    ///     a Formatter
    /// </summary>
    public struct FormattedLogEntry
    {
        public LogEntry Entry;
        public string FormattedEntry;
    }
    
}
