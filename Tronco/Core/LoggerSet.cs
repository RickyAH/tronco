using System.Collections;
using System.Collections.Generic;
using Tronco.Core.DataTypes;

namespace Tronco.Core
{
    /// <summary>
    ///     Represents a set of ILogger instances that can be retrieved using the 
    ///     ILogger.Name property value
    /// </summary>
    public class LoggerSet : ILoggerSet
    {
        BaseKeyValueContainer<string, ILogger> RegisteredLoggers { get; set; }

        public LoggerSet()
        {
            RegisteredLoggers = new BaseKeyValueContainer<string, ILogger>();
        }

        public bool Add(ILogger logger)
        {
            var result = RegisteredLoggers.Register(logger.Name, logger);
            if (result)
            {
                if (LogAdded != null) LogAdded(logger);
            }

            return result;
        }

        public bool Remove(ILogger logger)
        {
            var result = RegisteredLoggers.Remove(logger.Name);
            if (result != null)
            {
                if (LogRemoved != null) LogRemoved(result);
                return true;
            }

            return false;
        }
            
        public ILogger this[string name]
        {
            get
            {
                return RegisteredLoggers.Retrieve(name);
            }
        }

        public event LogEventHandler LogAdded;
        public event LogEventHandler LogRemoved;


        public IEnumerator<ILogger> GetEnumerator()
        {
            return RegisteredLoggers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}