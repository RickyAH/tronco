using System;
using Tronco.Filters;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tronco.Core
{
    /// <summary>
    ///     A Logger is the responsible of creating log entries and redirecting them
    ///     to the proper targers. 
    /// </summary>
    public class Logger : ILogger
    {
        /// <summary>
        ///     Notifyes whenever a log entry has been created. 
        ///     Logs targets added to this Logger register to this event
        ///     So they can process the log entries
        /// </summary>
        public event LogEntryCreatedHandler LogEntryCreated;

        /// <summary>
        ///     True if the logger is enabled
        /// </summary>
        /// <value><c>true</c> if this instance is enabled; otherwise, <c>false</c>.</value>
        public bool IsEnabled {get;set;}

        /// <summary>
        ///     Logger's name. It is treated as an unique id by a LogManager.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; private set; }

        /// <summary>
        ///     Minimum log level for this logger to actually create LogEntry instances.
        /// </summary>
        /// <value>The minimum level.</value>
        public ELogLevel MinimumLevel
        {
            get { return MinimumLogLevelFilter.MinimumLogLevel; }
            set { MinimumLogLevelFilter.MinimumLogLevel = value; }
        }

        public void Fatal(Func<string> deferredLog)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Fatal, deferredLog));
        }

        public void Fatal(string message, params object[] objs)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry( ELogLevel.Fatal, message, objs));
        }

        public void Error(Func<string> deferredLog)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Error, deferredLog));
        }

        public void Error(string message, params object[] objs)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Fatal, message, objs));
        }

        public void Warn(Func<string> deferredLog)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Warning, deferredLog));
        }

        public void Warn(string message, params object[] objs)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Warning, message, objs));
        }

        public void Info(Func<string> deferredLog)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Info, deferredLog));
        }

        public void Info(string message, params object[] objs)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Info, message, objs));
        }

        public void Debug(Func<string> deferredLog)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Debug, deferredLog));
        }

        public void Debug(string message, params object[] objs)
        {
            OnLogEntryCreated(CreateAndConfigureLogEntry(ELogLevel.Debug, message, objs));
        }

        public void MeasureBlock(string msg, Action block)
        {
            if (block == null) return;

            StartMeasuring(msg);
            block();
            StopMeasuring();
        }

        public void StartMeasuring(string message, params string[] args)
        {
            var pair = new MeasurementPair {
                DatTimer = new Stopwatch(),
                Msg = string.Format(message, args)
            };

            MeasurementTimersStack.Push(pair);
            pair.DatTimer.Start();
        }

        public void StopMeasuring()
        {
            if (MeasurementTimersStack.Count <= 0 ) return;

            var pair = MeasurementTimersStack.Pop();
            pair.DatTimer.Stop();
            OnLogEntryCreated(new MeasurementLogEntry(pair.DatTimer.Elapsed, pair.Msg));
        }

        #region Initialization
        public Logger(string name, ELogLevel level = ELogLevel.Debug) 
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException("name");

            // Set up filtering for minimum level in this logger
            MinimumLogLevelFilter = new MinimumLogLevelFilter(level);
            Name = name;
            IsEnabled = true;

            IgnoredTypesForFrameSkip = new List<Type>(new []{GetType() , typeof(LogEntry)});

            MeasurementTimersStack = new Stack<MeasurementPair>();
        }
        #endregion Initialization


        #region Internal Data
        MinimumLogLevelFilter MinimumLogLevelFilter {get; set;}
        protected List<Type> IgnoredTypesForFrameSkip {get;private set;}
        int? _cachedFrameSkip;
        #endregion



        #region Helpers

        protected int GetStackFrameSkip()
        {
            if (_cachedFrameSkip.HasValue) return _cachedFrameSkip.Value;

            var st = new StackTrace();
            for (int i = 0; i < st.FrameCount; ++i)
            {
                if (!IgnoredTypesForFrameSkip.Contains(st.GetFrame(i).GetMethod().DeclaringType))
                {
                    _cachedFrameSkip = i > 0? i-1 :0;
                    break;
                }
            }
                
            return _cachedFrameSkip.Value;
        }

        LogEntry CreateAndConfigureLogEntry(ELogLevel level, Func<string> deferredLog)
        {
            var entry = CreateLogEntry(level,deferredLog);

            entry.LoggerRef = this;
            entry.StackTraceSnapshot = new StackTrace(GetStackFrameSkip(), true);

            return entry;
        }

        LogEntry CreateAndConfigureLogEntry(ELogLevel level, string message, IEnumerable<object> objs)
        {
            var entry = CreateLogEntry(level, message, objs);

            entry.LoggerRef = this;
            entry.StackTraceSnapshot = new StackTrace(GetStackFrameSkip(), true);

            return entry;
        }

        #endregion

        #region Abstract Method Pattern
        /// <summary>
        ///     This methods creates a LogEntry instance.
        ///     Can be overriden by derived classes to change the way a LogEntry is created
        /// </summary>
        protected virtual LogEntry CreateLogEntry(ELogLevel level, Func<string> deferredLog)
        {
            return new LogEntry(level, deferredLog);
        }

        /// <summary>
        ///     This methods creates a LogEntry instance.
        ///     Can be overriden by derived classes to change the way a LogEntry is created
        /// </summary>
        protected virtual LogEntry CreateLogEntry(ELogLevel level, string message, IEnumerable<object> objs)
        {
            return new LogEntry(level, message, objs);
        }

        /// <summary>
        ///     Handler to the creation of a LogEntry. Can be overriden by a derived class
        ///     to change the way LogEntries are handled.
        /// </summary>
        /// <param name="logEntry">Log entry.</param>
        protected virtual void OnLogEntryCreated(LogEntry logEntry)
        {
            if (!IsEnabled || MinimumLogLevelFilter.ShouldDiscardLogEntry(logEntry)) return;

            if (LogEntryCreated != null) LogEntryCreated(logEntry);
        }
        #endregion

        protected struct MeasurementPair
        {
            public string Msg;
            public Stopwatch DatTimer;
        }
        protected Stack<MeasurementPair> MeasurementTimersStack {get; private set;}
    }
}