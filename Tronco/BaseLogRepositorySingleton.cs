using System.Collections.Generic;
using System;
using Tronco.Core;
using Tronco.Targets;

namespace Tronco
{
    /// <summary>
    ///     Base class that allows creating several a set of ILogRepository singleton instances
    ///     This allows to create multiple ILogRepositories globally accesible.
    /// </summary>
    public abstract class BaseLogRepositorySingleton<T> : ILogRepository where T : ILogRepository, new()
    {
        #region Singleton pattern
        static Dictionary<Type, ILogRepository> _instances = new Dictionary<Type, ILogRepository>();

        public static T Instance
        {
            get
            {   
                ILogRepository instance;
                if(!_instances.TryGetValue(typeof(T), out instance))
                {
                    instance = new T();
                    instance.InitDefaults();
                    _instances.Add(typeof(T), instance);
                }
                return (T)instance;
            }   
        }
        #endregion

        #region Private data
        ILoggerSet Loggers { get; set; }
        ILogTargetCollection Targets { get; set; }
        #endregion

        public BaseLogRepositorySingleton()
        {
            Loggers = new LoggerSet();
            Targets = new LogTargetCollection();
        }

        #region ILoggerRepository interface 


        public event LogEventHandler LogAdded
        {
            add 
            {
                Loggers.LogAdded += value;
            }
            remove 
            {
                Loggers.LogAdded -= value;
            }

        }

        public event LogEventHandler LogRemoved
        {
            add 
            {
                Loggers.LogRemoved += value;
            }
            remove 
            {
                Loggers.LogRemoved -= value;
            }
        }

        public bool AddTarget(ILogTarget target)
        {
            return Targets.RegisterTarget(target);
        }

        public bool AttachLoggerToTarget(string loggerName, string targetName)
        {
            var target = GetTarget(targetName);

            if(target == null) return false;

            var logger = GetLogger(loggerName);

            if (logger == null) return false;

            target.AttachLogger(logger);

            return true;
        }

        public ILogger CreateLogger(string name, ELogLevel logLevel)
        {
            var logger = GetLogger(name);
            if (logger == null)
            {
                logger = CreateLoggerInstance(name, logLevel);
                Loggers.Add(logger);
            }
                
            return logger;   
        }

        public ILogger DeleteLogger(string name)
        {
            var logger = GetLogger(name);
            if (logger != null)
            {
                Loggers.Remove(logger);

            }

            return logger;
        }

        public ILogger GetLogger(string name)
        {
            return Loggers[name];
        }

        public ILogTarget GetTarget(string name)
        {
            return Targets.GetTarget(name);
        }

        public bool HasLogger(string name)
        {
            return GetLogger(name) != null;
        }

        public bool HasTarget(string name)
        {
            return GetTarget(name) != null;
        }

        public ILogTarget RemoveTarget(string name)
        {
            var target = Targets.RemoveTarget(name);
            if ( target != null)
            {
                target.DettachAllLoggers();
            }

            return target;
        }
        #endregion 

        #region Abstract methods

        public virtual void InitDefaults() {}

        protected virtual ILogger CreateLoggerInstance(string name, ELogLevel logLevel)
        {
            return new Logger(name, logLevel);
        }

        #endregion

    }
}