using System;
using System.Collections.Generic;
using System.Linq;
using Tronco.Core;

namespace Tronco.Filters
{
    public class LogEntryFilterCollection : ILogEntryFilterCollection
    {
        readonly List<ILogEntryFilter> _discardFilters;

        public LogEntryFilterCollection()
        {
            _discardFilters = new List<ILogEntryFilter>();
        }

        public IEnumerable<LogEntry> FilterEntries(LogEntry logEntry)
        {
            return FilterEntries(new[] { logEntry });
        }

        public IEnumerable<LogEntry> FilterEntries(IEnumerable<LogEntry> logEntries)
        {
            // Discard log entries that would be rejected by any of the registered filters
            return logEntries.Where(logEntry =>
                                    !_discardFilters.Any(filter => filter.ShouldDiscardLogEntry(logEntry)));
        }

        public ILogEntryFilterCollection AddFilters(params ILogEntryFilter[] filters)
        {
            _discardFilters.AddRange(filters);

            return this;
        }

        /// <summary>
        ///     This method allows setting multiple lambdas that check if a LogEntry should be filtered out
        /// </summary>
        /// <returns>The reject if filter.</returns>
        /// <param name="discardConditions">Discard conditions.</param>
        public ILogEntryFilterCollection AddRejectIfFilter(params Func<LogEntry, bool>[] discardConditions)
        {
            _discardFilters.AddRange(discardConditions.Select(c => (ILogEntryFilter) new LambdaFilter(c)));

            return this;
        }
    }
}