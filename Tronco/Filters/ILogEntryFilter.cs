using Tronco.Core;

namespace Tronco.Filters
{
    /// <summary>
    ///     Interface for a LogEntry Filterer that allows discargin LogEntry instances 
    ///     given user-defined constraints.
    /// </summary>
    public interface ILogEntryFilter
    {
        /// <summary>
        ///     Computes if a LogEntry should be discarted and not sent to any target,
        ///     which prevents the entry from being logged at all.
        /// </summary>
        bool ShouldDiscardLogEntry(LogEntry logEntry);
    }
}