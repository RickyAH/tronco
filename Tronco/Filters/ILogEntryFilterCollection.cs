using System;
using System.Collections.Generic;
using Tronco.Core;

namespace Tronco.Filters
{
    /// <summary>
    ///     Collection of LogEntry filters. 
    ///     Allow to store and apply multiple filters over multiple LogEntry instances.
    /// </summary>
    public interface ILogEntryFilterCollection
    {
        /// <summary>
        ///     Given a LogEntry filter that instance if any of the rejection filters in 
        ///     this collection returns true. If filtered this returns an empty IEnumerable<LogEntry>
        ///     If not filtered, returns an IEnumerable<LogEntry> with the LogEntry that
        ///     was passed as parameter.
        /// </summary>

        IEnumerable<LogEntry> FilterEntries(LogEntry logEntry);

        /// <summary>
        ///     Given a collection of LogEntry instances, filters those instances
        ///     if any of the rejection filters in this collections returns true and
        ///     returns a new collection without the filtered LogEntry instances.
        /// </summary>
        IEnumerable<LogEntry> FilterEntries(IEnumerable<LogEntry> logEntries);

        /// <summary>
        ///     Adds a collection of filters to be performed over a set of LogEntry instances.
        /// </summary>
        ILogEntryFilterCollection AddFilters(params ILogEntryFilter[] filters);

        /// <summary>
        ///     This method allows setting multiple lambdas that check if a LogEntry should be filtered out
        /// </summary>
        /// <param name="discardConditions">
        ///     Quick filter predicate for a LogEntry, using an Func that returns true if the LogEntry is
        ///     to be discarted.
        /// </param>
        ILogEntryFilterCollection AddRejectIfFilter(params Func<LogEntry, bool>[] discardConditions);
    }
}