using Tronco.Core;

namespace Tronco.Filters
{

    /// <summary>
    ///     Filters a LogEntry if its Log Level does not match a minimum Level value
    /// </summary>
    public class MinimumLogLevelFilter : ILogEntryFilter
    {
        public ELogLevel MinimumLogLevel { get; set; }

        public MinimumLogLevelFilter(ELogLevel minimumLevel)
        {
            MinimumLogLevel = minimumLevel;
        }

        public bool ShouldDiscardLogEntry(LogEntry logEntry)
        {
            return logEntry.LogLevel < MinimumLogLevel;
        }
    }
}