using System;
using Tronco.Core;

namespace Tronco.Filters
{
    /// <summary>
    ///     The LambdaFilter allows to create an ILogEntryFilter using
    ///     a lambda to define a predicate rule that the LogEntry must 
    ///     comply to be discarted.
    /// </summary>
    public class LambdaFilter : ILogEntryFilter
    {
        readonly Func<LogEntry, bool> _discardPredictate;

        public LambdaFilter(Func<LogEntry, bool> discardPredictate)
        {
            _discardPredictate = discardPredictate;
        }

        public bool ShouldDiscardLogEntry(LogEntry logEntry)
        {
            return _discardPredictate(logEntry);
        }

        public override int GetHashCode()
        {
            return _discardPredictate.GetHashCode();
        }
    }
}