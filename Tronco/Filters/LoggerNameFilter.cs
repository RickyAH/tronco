using System;
using Tronco.Core;

namespace Tronco.Filters
{
    /// <summary>
    ///     Filters a LogEntry if the Logger that created it it does not matches a given name
    /// </summary>
    public class LoggerNameFilter : ILogEntryFilter
    {
        public string LoggerName { get; private set; }

        public LoggerNameFilter(string loggerName)
        {
            if (string.IsNullOrEmpty(loggerName)) throw new ArgumentNullException("loggerName");
            LoggerName = loggerName;
        }

        public bool ShouldDiscardLogEntry(LogEntry logEntry)
        {
            return logEntry.LoggerRef.Name != LoggerName;
        }
    }
}