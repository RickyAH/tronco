using System;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Tronco.Helpers
{
    /// <summary>
    ///     Allows formatting an string, similar to String.Format, but using an object's properties names
    ///     as indexes instead of numbers.
    /// </summary>
    /// <remarks>
    ///     Code taken from http://www.hanselman.com/blog/CommentView.aspx?guid=fde45b51-9d12-46fd-b877-da6172fe1791
    /// </remarks>
    public class FormatStringKeywordMatcher
    {
        const string _regexSpecialChars = @"\.$^{[(|)*+?";
        string _startMatchRegexStr;
        string _endMatchRegexStr;

        public FormatStringKeywordMatcher() :this("{", "}")
        {
        }

        public FormatStringKeywordMatcher(string startMatchRegexStr, string endMatchRegexStr)
        {
            if (string.IsNullOrEmpty(startMatchRegexStr)) 
                throw new ArgumentNullException("startMatchRegexStr");
            if (string.IsNullOrEmpty(endMatchRegexStr)) 
                throw new ArgumentNullException("endMatchRegexStr");

            _startMatchRegexStr = ReplaceSpecialChars(startMatchRegexStr);
            _endMatchRegexStr = ReplaceSpecialChars(endMatchRegexStr);
        }

        private string ReplaceSpecialChars(string strToSanitarize)
        {
            foreach(var specialChar in _regexSpecialChars)
            {
                strToSanitarize = strToSanitarize.Replace(specialChar.ToString(), @"\" + specialChar);
            }

            return strToSanitarize;
        }

 
        public string FormatWithObject(string aFormat, object anObject, IFormatProvider formatProvider = null)
        {
            var sb = new StringBuilder();
            var type = anObject.GetType();

            string startMatchStr = _startMatchRegexStr.Replace(@"\", string.Empty);
            string endMatchStr = _endMatchRegexStr.Replace(@"\", string.Empty);

            var reg = new Regex(string.Format(@"({0})([^{1}]+)({1})", _startMatchRegexStr, _endMatchRegexStr), RegexOptions.IgnoreCase);
            
            int startIndex = 0;
            string toFormat = String.Empty;
            string toGet;

            foreach (Match m in reg.Matches(aFormat))
            {
                Group g = m.Groups[2]; //it's second in the match between { and }  
                int length = g.Index - startIndex - startMatchStr.Length;
                sb.Append(aFormat.Substring(startIndex, length));

                int formatIndex = g.Value.IndexOf(":"); //formatting would be to the right of a :  
                if (formatIndex == -1) //no formatting, no worries  
                {
                    toGet = g.Value;
                }
                else //pickup the formatting  
                {
                    toGet = g.Value.Substring(0, formatIndex);
                    toFormat = g.Value.Substring(formatIndex + 1);
                }

                //first try properties  
                PropertyInfo retrievedProperty = type.GetProperty(toGet);
                Type retrievedType = null;
                object retrievedObject = null;
                if (retrievedProperty != null)
                {
                    retrievedType = retrievedProperty.PropertyType;
                    retrievedObject = retrievedProperty.GetValue(anObject, null);
                }
                else //try fields  
                {
                    FieldInfo retrievedField = type.GetField(toGet);
                    if (retrievedField != null)
                    {
                        retrievedType = retrievedField.FieldType;
                        retrievedObject = retrievedField.GetValue(anObject);
                    }
                }

                if (retrievedType != null) //Cool, we found something  
                {
                    string result;
                    if (toFormat == String.Empty) //no format info  
                    {
                        result = retrievedType.InvokeMember("ToString",
                            BindingFlags.Public | BindingFlags.NonPublic |
                            BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.IgnoreCase
                            , null, retrievedObject, null) as string;
                    }
                    else //format info  
                    {
                        result = retrievedType.InvokeMember("ToString",
                            BindingFlags.Public | BindingFlags.NonPublic |
                            BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.IgnoreCase
                            , null, retrievedObject, new object[] { toFormat, formatProvider }) as string;
                    }
                    sb.Append(result);
                }
                else //didn't find a property with that name, so be gracious and put it back  
                {
                    sb.Append(startMatchStr);
                    sb.Append(g.Value);
                    sb.Append(endMatchStr);
                }
                startIndex = g.Index + g.Length + endMatchStr.Length;
            }
            if (startIndex < aFormat.Length) //include the rest (end) of the string  
            {
                sb.Append(aFormat.Substring(startIndex));
            }
            return sb.ToString();
        }
    }
}
