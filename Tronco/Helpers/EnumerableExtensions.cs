using System;
using System.Collections.Generic;

namespace Tronco.Helpers
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> elements, Action<T> action)
        {
            foreach (var element in elements) action(element);
        } 
    }
}