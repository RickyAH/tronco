using Tronco.Core;
using Tronco.Targets;

namespace Tronco
{
    /// <summary>
    ///     A log repository allows to create, retrieve and delete
    ///     ILogger and ILogTargets instances from a central point. 
    ///     Its just a convienence class to ease the managemente of both
    ///     loggers and targets
    /// </summary>
    public interface ILogRepository
    {   
        /// <summary>
        ///     Initialices default data for the repository. 
        /// </summary>
        void InitDefaults();

        /// <summary>
        ///     Attachs a logger registered in this repository with a target
        ///     also previously registered in this repository
        /// </summary>
        /// <remarks>
        ///     This operation will fail if either no logger or no target with the
        ///     given name can be found in this repository
        /// </remarks>
        /// <param name="loggerName">Name of an ILogger registered in this repository.</param>
        /// <param name="targetName">Name of an ITarget registered in this repository.</param>
        /// <returns>
        ///     <c>true</c>, if the logger with that name was successfully attached to
        ///     the target with the specified name; otherwise returns <c>false</c>
        /// </returns>
        bool AttachLoggerToTarget(string loggerName, string targetName);

        /// <summary>
        ///     Registers a target instance in this repository.
        /// </summary>
        bool AddTarget(ILogTarget target);

        /// <summary>
        ///     Creates a new logger with a given name and a given log level.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="logLevel"></param>
        /// <returns>
        ///     A newly created Logger instance. If a Logger with the same name
        ///     was already registered in this log manager, returns the existing
        ///     instance.
        /// </returns>
        ILogger CreateLogger(string name, ELogLevel logLevel);

        /// <summary>
        ///     Removes a logger with a given name
        /// </summary>
        /// <param name="name">
        /// </param>
        /// <returns></returns>
        ILogger DeleteLogger(string name);
        
        /// <summary>
        ///     Retrieves an existing ILogger with a given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>
        ///     Returns the logger instance or null if it does not exists.
        /// </returns>
        ILogger GetLogger(string name);


        /// <summary>
        ///     Retrieves an existing ILogTarget with a given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>
        ///     Returns the ILogTarget instance or null if it does not exists.
        /// </returns>
        ILogTarget GetTarget(string name);

        /// <summary>
        ///     Allows to check if an ILogger with a given name exists in this repository.
        /// </summary>
        bool HasLogger(string name);

        /// <summary>
        ///     Allows to check if an ILogTarget with a given name exists in this repository.
        /// </summary>
        bool HasTarget(string name);

        /// <summary>
        ///     Removes a target with the given name. 
        ///     This makes that all loggers registered in the removed target
        ///     to be dettached.
        /// </summary>
        /// <returns>
        ///     Returns the removed target with its loggers dettached
        ///     or null if a target with that name could not be found.
        /// </returns>
        ILogTarget RemoveTarget(string name);

        /// <summary>
        ///     Raised when a new log is added to the collection
        /// </summary>
        event LogEventHandler LogAdded;

        /// <summary>
        ///     Raised when a log is removed from the collection
        /// </summary>
        event LogEventHandler LogRemoved;
    }
}