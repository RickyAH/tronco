namespace Tronco.Outputs.FileOutputs
{
    /// <summary>
    ///     Allows configuring the behaviour of a FileLogOutput instance.
    /// </summary>
    public class FileLogOutputConfig
    {
        /// <summary>
        ///     Returns the default configuration settings.
        /// </summary>
        /// <value>The default.</value>
        public static FileLogOutputConfig Default
        {
            get 
            {
                return new FileLogOutputConfig();
            }
        }

        /// <summary>
        ///     Overwrite the log file in each logging session
        /// </summary>
        /// <remarks>
        ///     Defaults to false
        /// </remarks>
        public bool OverwriteFileOnNewSession {get; set;}

        /// <summary>
        ///     When a new session starts automatically append as the first line in the file
        ///     the date and time when the session started
        /// </summary>
        /// <remarks>
        ///     Defaults to true
        /// </remarks>
        public bool AppendDateAndTimeOnNewSession {get;set;}

        public FileLogOutputConfig()
        {
            AppendDateAndTimeOnNewSession = true;
        }

    }
    
}