using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Tronco.Core;

namespace Tronco.Outputs.FileOutputs
{
    public class FileLogOutput : ILogOutput, IDisposable
    {
        public bool IsOpened {get; private set;}
        public string FilePath {get; set;}
        FileLogOutputConfig Config { get; set; }

        StreamWriter _streamWriter;

        public FileLogOutput(string filePath) 
            : this(filePath, null)
        {
         
        }

        public FileLogOutput(string filePath, FileLogOutputConfig config)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("FileLogOutputConfig");

            if (config == null)
                config = FileLogOutputConfig.Default;

            Config = config;
            FilePath = filePath;

            Initialize();
        }

        public void Open()
        {
            if (!IsOpened)
                Initialize();
        }
        
        public void Close()
        {
            Flush();

            if (IsOpened)
                _streamWriter.Close();

            IsOpened = false;
        }
        
        public void Flush()
        {
            if (IsOpened)
                _streamWriter.Flush();
        }
            
        void Initialize(bool forceInitialization=false)
        {
            if (IsOpened || forceInitialization) Close();

            if (Config.OverwriteFileOnNewSession) 
            {
                _streamWriter = CreateNewLogFile(FilePath);
            }
            else
            {
                _streamWriter = OpenExistingLogFileAndAppend(FilePath);
            }
                
            if(Config.AppendDateAndTimeOnNewSession)
            {
                _streamWriter.WriteLine(NewSessionLine);
            }

            IsOpened = true;
        }

        #region ILogOutput implementation

        public void OutputEntries(IEnumerable<FormattedLogEntry> logEntries)
        {
            if (!IsOpened) return;

            foreach(var entry in logEntries)
                _streamWriter.WriteLine(entry.FormattedEntry);
        }

        #endregion

        string NewSessionLine
        {
            get {return string.Format("--- Session started on {0} ---", System.DateTime.Now);}
        }


        public void Dispose()
        {
            Close();
        }
            
        static StreamWriter OpenExistingLogFileAndAppend(string filePath)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            return new StreamWriter(filePath, true, Encoding.UTF8);
        }

        static StreamWriter CreateNewLogFile(string filePath)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            EmptyFile(filePath);

            return new StreamWriter(filePath,false, Encoding.UTF8);
        }

        static void EmptyFile(string filePath)
        {
            File.Create(filePath).Close();
        }

    }
}