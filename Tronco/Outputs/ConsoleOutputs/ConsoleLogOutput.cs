using System.Collections.Generic;
using Tronco.Helpers;
using Tronco.Core;

namespace Tronco.Outputs.ConsoleOutputs
{
    public class ConsoleLogOutput : ILogOutput
    {
        public void OutputEntries(IEnumerable<FormattedLogEntry> logEntries)
        {
            logEntries.ForEach(logEntry => System.Console.WriteLine(logEntry.FormattedEntry));           
        }
    }
}