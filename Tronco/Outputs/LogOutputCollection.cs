using System.Collections.Generic;
using Tronco.Helpers;
using Tronco.Core.DataTypes;
using Tronco.Core;


namespace Tronco.Outputs
{
    public class LogOutputCollection : BaseKeyValueContainer<int, ILogOutput>, ILogOutputCollection
    {
        BaseKeyValueContainer<int, ILogOutput> Outputs { get; set; }

        public LogOutputCollection()
        {
            Outputs = new BaseKeyValueContainer<int, ILogOutput>();
        }

        public bool Add(ILogOutput transporter)
        {
            return Outputs.Register(transporter.GetHashCode(), transporter);
        }

        public bool Remove(ILogOutput transporter)
        {
            return Outputs.Remove(transporter.GetHashCode()) != null;
        }

        public void OutputEntries(IEnumerable<FormattedLogEntry> logEntries)
        {
            Outputs.ForEach(output => output.OutputEntries(logEntries));
        }


    }
}