﻿using System.Collections.Generic;
using Tronco.Core;

namespace Tronco.Outputs
{
    /// <summary>
    ///     This defines where a LogEntry is to be persisted after it has been formatted.
    /// </summary>
    public interface ILogOutput
    {
        /// <summary>
        ///     Persists a collection of FormattedLogEntry instances.
        /// </summary>
        void OutputEntries(IEnumerable<FormattedLogEntry> logEntries);
    }
}
