using System.Collections.Generic;

namespace Tronco.Outputs
{
    /// <summary>
    ///     Stores a collection of ILogOutputs, and it is also a ILogOutput, which allows using
    ///     this to make a Logger outputs to multiple end points.
    /// </summary>
    /// <remarks>
    ///     Uses the Composite design pattern
    /// </remarks>
    public interface ILogOutputCollection : ILogOutput, IEnumerable<ILogOutput>
    {
        bool Add(ILogOutput transporter);
        bool Remove(ILogOutput transporter);
    }
}