using System;
using System.Net.Sockets;
using System.Net;
using System.Text;
using Tronco.Outputs;

namespace Tronco.Outputs.UDPOutputs
{
    /// <summary>
    ///     Configuration for an Udp logger Output
    /// </summary>
    public class UdpOutputConfiguration
    {
        /// <summary>
        ///     Port where the data will be sent to
        /// </summary>
        /// <value>The port.</value>
        public int Port { get; private set;}

        /// <summary>
        ///     Endpoint ip address
        /// </summary>
        /// <value>The ip address.</value>
        public IPAddress IpAddress {get; private set;}

        /// <summary>
        ///     Returns true if the endpoint ip adress is a broadcast addreess
        /// </summary>
        public bool DoesSendToBroadcast
        {
            get
            {
                foreach(byte b in IpAddress.GetAddressBytes())
                {
                    if (b == 0xFF) return true;
                }

                return false;
            }
        }

        /// <summary>
        ///     Default configuration values (broadcast to port 11000)
        /// </summary>
        /// <value>The defaults.</value>
        public static UdpOutputConfiguration Defaults
        {
            get
            {
                return new UdpOutputConfiguration(11000);
            }
        }

        #region Initialization
        public UdpOutputConfiguration(IPAddress ipAddress, int port)
        {
            Port = port;
            IpAddress = ipAddress;
        }

        public UdpOutputConfiguration(string ipAddress, int port)
        {
            Port = port;
            IpAddress = SelectIpAddressFromString(ipAddress);
        }

        public UdpOutputConfiguration(int port):this(string.Empty, port)
        {

        }
        #endregion

        #region helpers

        static IPAddress SelectIpAddressFromString(string ipAddress)
        {
            if (!string.IsNullOrEmpty(ipAddress) )
            {
                return IPAddress.Parse(ipAddress);
            }
             
            return GetBroadcastIpForCurrentHost();
        }

        static IPAddress GetBroadcastIpForCurrentHost()
        {
            IPAddress[] addresses = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
            if (addresses.Length <= 0) return IPAddress.Broadcast;

            byte[] ipBytes = addresses[0].GetAddressBytes();

            ipBytes[3] = 0xFF;

            return new IPAddress(ipBytes);
        }
        #endregion

    }
    
}

/*
* The purpose of this program is to provide a minimal example of using UDP to
* send data.
* It transmits broadcast packets and displays the text in a console window.
* This was created to work with the program UDP_Minimum_Listener.
* Run both programs, send data with Talker, receive the data with Listener.
*/
