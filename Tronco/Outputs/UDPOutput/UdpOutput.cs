﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Text;
using Tronco.Outputs;

namespace Tronco.Outputs.UDPOutputs
{
    /// <summary>
    ///     Logs entries as UDP packets using broadcast packets or
    ///     specific endpoints.
    /// </summary>
    public class UdpOutput : ILogOutput, IDisposable
    {
        #region Private data
        Socket SendSocket;
        IPEndPoint EndPoint;
        #endregion

        public UdpOutputConfiguration Configuration {get; private set;}

        #region initialization
        public UdpOutput() :this(UdpOutputConfiguration.Defaults) {}

        public UdpOutput(UdpOutputConfiguration configuration)
        {
            SendSocket = new Socket( AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            SendSocket.EnableBroadcast = configuration.DoesSendToBroadcast;
            EndPoint = new IPEndPoint(configuration.IpAddress, configuration.Port);
        }
        #endregion 

        #region ILogOutput implementation
        public void OutputEntries(System.Collections.Generic.IEnumerable<Tronco.Core.FormattedLogEntry> logEntries)
        {
            foreach(var logEntry in logEntries)
            {
                byte[] sendbuf = Encoding.UTF8.GetBytes(logEntry.FormattedEntry);

                SendSocket.SendTo(sendbuf, EndPoint);
            }
        }

        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            SendSocket.Close();
        }

        #endregion
    }
}
