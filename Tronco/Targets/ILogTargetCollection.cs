using System.Collections.Generic;

namespace Tronco.Targets
{
    public interface ILogTargetCollection : IEnumerable<ILogTarget>
    {
        bool RegisterTarget(ILogTarget target);
        ILogTarget RemoveTarget(string name);
        ILogTarget GetTarget(string targetName);
    }
}