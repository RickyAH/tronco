using System;
using Tronco.Core;
using Tronco.Filters;
using Tronco.Formatters;
using Tronco.Outputs;

namespace Tronco.Targets
{
    public class LogTarget : ILogTarget, IDisposable
    {
        public LogTarget(string name) :this(name, null)
        {
        }

        public LogTarget(string name, ILogEntryFormatter formatter)
        {
            if (string.IsNullOrEmpty(name))
                throw new NullReferenceException("name");

            if (formatter == null)
                formatter = new DefaultLogEntryFormatter();

            Name = name;
            Formatter = formatter;
            Filters = new LogEntryFilterCollection();
            Outputs = new LogOutputCollection();
            Loggers = new LoggerSet();
        }

        public string Name { get; private set; }

        public ILoggerSet Loggers {get; private set;}
        public ILogEntryFormatter        Formatter  { get; set; }
        public ILogEntryFilterCollection Filters    { get; private set; }
        public ILogOutputCollection      Outputs    { get; private set; }

        public virtual void AttachLogger(ILogger logger)
        {
            if(Loggers.Add(logger))
            {
                logger.LogEntryCreated += OutputLogEntry;
            }
        }

        public virtual void DettachLogger(ILogger logger)
        {
            if(Loggers.Remove(logger))
            {
                logger.LogEntryCreated -= OutputLogEntry;
            }
        }

        public virtual void DettachLogger(string loggerName)
        {
            var logger = Loggers[loggerName];
            if (logger== null) return;

            DettachLogger(logger);

        }

        public virtual void DettachAllLoggers()
        {
            var collection = new System.Collections.Generic.List<ILogger>(Loggers);

            foreach(var logger in collection)
            {
                DettachLogger(logger);
            }
        }

        #region IDisposable implementation

        public void Dispose()
        {
            DettachAllLoggers();
        }

        #endregion

        protected virtual void OutputLogEntry(LogEntry logEntry)
        {
            Outputs.OutputEntries(Formatter.FormatLogEntries(Filters.FilterEntries(new []{logEntry})));
        }
            
    }
}