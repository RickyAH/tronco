using Tronco.Filters;
using Tronco.Formatters;
using Tronco.Outputs;
using Tronco.Core;

namespace Tronco.Targets
{
    /// <summary>
    ///     An ILogTarget is used an aggregated structure for the log system, consisting of a set of filter rules,
    ///     one formatter, a set of outputs and the collection of loggers that use the target. 
    ///     A logger can be  attached to one or more ILogTarget instances; after being attached, the ILogTarget will
    ///     capture all the LogEntry instances the ILogger generates, filtering, formatting and sending those entries,
    ///     to the to one or more outputs.
    /// </summary>
    public interface ILogTarget
    {
        string Name { get; }
        ILoggerSet Loggers {get;}
        /// <summary>
        ///     Filters allows us to define a set of arbitrary rules for filtering LogEntries  (e.g. we could 
        ///     filter LogEntry with an specifice ELogLevel)
        /// </summary>
        ILogEntryFilterCollection Filters { get; }

        /// <summary>
        ///     The formatter defines how to to transform the data in a LogEntry to a string.
        /// </summary>
        ILogEntryFormatter Formatter { get; }

        /// <summary>
        ///     Outputs define where the LogEntries will be sent for representation or to be persisted
        ///     (e.g. printed to  console, or saved to a file) 
        /// </summary>
        ILogOutputCollection Outputs { get; }

        /// <summary>
        ///     Attachs an ILogger instance to this target
        /// </summary>
        void AttachLogger(ILogger logger);

        /// <summary>
        ///     Deattachs an ILogger instance from this target
        /// </summary>
        void DettachLogger(ILogger logger);
        /// <summary>
        ///     Deattachs an ILogger instance from this target given a target's name
        /// </summary>
        void DettachLogger(string loggerName);

        /// <summary>
        /// Dettachs all ILogger instances from this target.
        /// </summary>
        void DettachAllLoggers();
    }
}