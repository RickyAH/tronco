using System.Collections;
using System.Collections.Generic;
using Tronco.Core.DataTypes;

namespace Tronco.Targets
{
    public class LogTargetCollection : ILogTargetCollection
    {
        public BaseKeyValueContainer<string, ILogTarget> Targets {get; private set; }

        public LogTargetCollection()
        {
            Targets = new BaseKeyValueContainer<string, ILogTarget>();
        }

        public bool RegisterTarget(ILogTarget target)
        {
            return Targets.Register(target.Name, target);
        }

        public ILogTarget RemoveTarget(string name)
        {
            return Targets.Remove(name);
        }

        public ILogTarget GetTarget(string targetName)
        {
            return Targets.Retrieve(targetName);
        }

        public IEnumerator<ILogTarget> GetEnumerator()
        {
            return Targets.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}