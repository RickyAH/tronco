using System;
using System.Collections.Generic;
using System.Linq;
using Tronco.Core;
using Tronco.Helpers;

namespace Tronco.Formatters
{
    /// <summary>
    ///     Allows creating log string enclosing the keywords between 
    ///     curly braces as delimiters ( { and } )
    /// </summary>
    public class DefaultLogEntryFormatter : ILogEntryFormatter
    {
        // Used to match data between a format string and a LogEntry
        protected FormatStringKeywordMatcher _keywordMatcher;
        
        // Format string used if none provider.
        const string _defaultFormatString = @"{level} - {timestamp} - {message}";
        const string _defaultMeasurementFormatString = @"{message} {elapsedtime}";
        /// <summary>
        ///     Ctor.
        /// </summary>
        public DefaultLogEntryFormatter():this(_defaultFormatString)
        {
        }

        /// <summary>
        ///     Ctor.
        /// </summary>
        /// <param name="formatMessageString"></param>
        public DefaultLogEntryFormatter(string formatMessageString)
        {
            if (string.IsNullOrEmpty(formatMessageString))
                throw new ArgumentNullException("formatMessageString");

            FormatString = formatMessageString;


            _keywordMatcher = new FormatStringKeywordMatcher();
        }

        /// <summary>
        ///     Returns the format string used by default if none is provided
        /// </summary>
        public virtual string DefaultFormatString
        {
            get { return _defaultFormatString; }
        }

        public string MeasurementFormatString
        {
            get
            {
                return _defaultMeasurementFormatString;
            }
        }

        /// <summary>
        ///     String that defines the structure of the final log string
        ///     that will be outputted. 
        ///     Allows defining placeholders for data contained in the LogEntry
        ///     that will be used when creating a log string. 
        ///     The data available depends on the specific implementation, but
        ///     should contain the log message, the LogEntry timestamp, stacktrace
        ///     info, and such.
        /// </summary>
        public string FormatString { get; set; }

        /// <summary>
        ///     This method creates a log string given a LogEntry instance
        ///     The structure of the created message conforms to the defined
        ///     in the format string.
        /// </summary>
        /// <param name="logEntry"></param>
        /// <returns></returns>
        public virtual FormattedLogEntry FormatLogEntry(LogEntry logEntry)
        {
            try
            {
                string formatString = FormatString;
                if (logEntry is MeasurementLogEntry)
                {
                    formatString = MeasurementFormatString;
                }

                return new FormattedLogEntry
                {
                    Entry = logEntry,
                    FormattedEntry = _keywordMatcher.FormatWithObject(formatString, logEntry.GetInternalData())
                };
            }
            catch(Exception ex)
            {
                return new FormattedLogEntry
                {
                    Entry = logEntry,
                    FormattedEntry = string.Format("Bad formed format string: [{0}] {1} LogEntry:{2}{3}", 
                        FormatString,
                        ex,
                        Environment.NewLine,
                        logEntry)
                };
            }

        }

        public IEnumerable<FormattedLogEntry> FormatLogEntries(IEnumerable<LogEntry> filterEntries)
        {
            return filterEntries.Select( e => FormatLogEntry(e) );
        }
    }
}