using Tronco.Helpers;

namespace Tronco.Formatters
{
    /// <summary>
    ///     Allows creating log string enclosing the keywords using
    ///     NLog delimiters ${ and }
    /// </summary>
    public class NLogEntryFormatter : DefaultLogEntryFormatter
    {
        const string _defaultFormatString = @"${level} - ${timestamp} - ${message}";

        public override string DefaultFormatString
        {
            get { return _defaultFormatString; }
        }

        public NLogEntryFormatter():this(_defaultFormatString)
        {
        }

        public NLogEntryFormatter(string formatMessageString)
            : base(formatMessageString)
        {
            _keywordMatcher = new FormatStringKeywordMatcher("${", "}");
        }
    }
}