using System.Collections.Generic;
using Tronco.Core;

namespace Tronco.Formatters
{
    /// <summary>
    ///     Interface for a formatter of a LogEntry.
    ///     The formatter needs a FormatString which contains the 
    ///     placeholders that will be replaced by actual values from
    ///     the LogEntry when it is evaluated by this formatter
    /// </summary>
    /// <example>
    ///     Using the default formatter and the following format string:
    ///     "{timestamp} {message}"
    ///     When you call FormatLogEntry, the final message will contain
    ///     the timestam when the LogEntry was generated, and the message
    ///     used to create the log entry.
    /// </example>
    public interface ILogEntryFormatter
    {
        /// <summary>
        ///     Returns the format string used by default if none is provided
        /// </summary>
        string DefaultFormatString { get; }

        /// <summary>
        ///     String that defines the structure of the final log string
        ///     that will be outputted. 
        ///     Allows defining placeholders for data contained in the LogEntry
        ///     that will be used when creating a log string. 
        ///     The data available depends on the specific implementation, but
        ///     should contain the log message, the LogEntry timestamp, stacktrace
        ///     info, and such.
        /// </summary>
        string FormatString { get; set; }

        /// <summary>
        ///     Returns the format string to use when formatting MeasurementeLogEntry instances
        /// </summary>
        string MeasurementFormatString { get; }

        /// <summary>
        ///     This method creates a log string given a LogEntry instance
        ///     The structure of the created message conforms to the defined
        ///     in the format string.
        /// </summary>
        /// <param name="logEntry"></param>
        /// <returns></returns>
        FormattedLogEntry FormatLogEntry(LogEntry logEntry);

        /// <summary>
        ///     Formats a collection of log entries
        /// </summary>
        /// <param name="filterEntries"></param>
        /// <returns></returns>
        IEnumerable<FormattedLogEntry> FormatLogEntries(IEnumerable<LogEntry> filterEntries);
    }
}