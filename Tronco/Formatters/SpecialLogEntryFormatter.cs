using Tronco.Core;
using Tronco.Helpers;

namespace Tronco.Formatters
{
    /// <summary>
    ///     Allows creating log string enclosing the keywords usins
    ///     the delimiters $( and )
    /// </summary>
    public class SpecialLogEntryFormatter : DefaultLogEntryFormatter
    {
        const string _defaultFormatString = @"$(level) - $(timestamp) - $(message)";
        public override string DefaultFormatString
        {
            get { return _defaultFormatString; }
        }


        public SpecialLogEntryFormatter(): this(_defaultFormatString){}

        public SpecialLogEntryFormatter(string formatMessageString)
            :base(formatMessageString)
        {
            _keywordMatcher = new FormatStringKeywordMatcher("$(", ")");
        }
    }
}