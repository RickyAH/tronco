using System;
using Tronco.Core;
using System.Collections.Generic;


namespace Tronco.Unity3D
{
    public class UnityLogger : Logger
    {
        public UnityLogger(string name, ELogLevel level = ELogLevel.Debug)
            :base(name, level)
        {
            IgnoredTypesForFrameSkip.Add(GetType());
            IgnoredTypesForFrameSkip.Add(typeof(UnityLogEntry));
        }

        protected override LogEntry CreateLogEntry(ELogLevel level, Func<string> deferredLog)
        {
            return new UnityLogEntry(level, deferredLog);
        }

        protected override LogEntry CreateLogEntry(ELogLevel level, string message, IEnumerable<object> objs)
        {
            return new UnityLogEntry(level, message, objs);
        }
    }
}