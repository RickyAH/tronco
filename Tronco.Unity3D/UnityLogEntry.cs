﻿using System;
using Tronco.Core;
using System.Collections.Generic;


namespace Tronco.Unity3D
{

    public class UnityLogEntry : LogEntry
    {
        public UnityLogEntry(ELogLevel level)
            : base(level)
        {}

        public UnityLogEntry(ELogLevel level, string formatMsgStringMsg, IEnumerable<object> objs)
            : base(level, formatMsgStringMsg, objs)
        {}

        public UnityLogEntry(ELogLevel level, Func<string> createMsgDelegate)
            : base(level, createMsgDelegate)
        {}

        public int FrameNumber
        {
            get
            {
                return UnityEngine.Time.frameCount;
            }
        }

        public override object GetInternalData()
        {
            return new
            {
                level = LogLevel,
                timestamp = Timestamp,
                message = Message,
                stacktrace = StackTraceSnapshot,
                stackframe = StackFrameSnapshot,
                newline = Environment.NewLine,
                time_now = DateTime.Now,
                method = MethodName,
                filename = FileName ?? string.Empty,
                linenumber = LineNumber > 0 ? LineNumber.ToString() : string.Empty,
                frame = FrameNumber
            };
        }

    }
}
