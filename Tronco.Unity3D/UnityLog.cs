using Tronco.Core;
using Tronco.Unity3D.Outputs;
using Tronco.Targets;

namespace Tronco.Unity3D
{
    public class ULog : BaseLogRepositorySingleton<ULog>
    {
        const string Unity3DConsoleTargetName = "u3dconsole_target";
        const string Unity3DDefaultLoggerName = "u3ddefault_logger";

        public static ILogger Default 
        {
            get 
            {
                return Instance.GetLogger(Unity3DDefaultLoggerName);
            }
        }

        public bool AttachLoggerToUnityConsole(string loggerName)
        {
            return AttachLoggerToTarget(loggerName, Unity3DConsoleTargetName);
        }

        public override void InitDefaults()
        {
            // Create a unity3d console target by default
            var target = new LogTarget(Unity3DConsoleTargetName);
            target.Outputs.Add(new Unity3DConsoleOutput());

            AddTarget(target);

            CreateLogger(Unity3DDefaultLoggerName, ELogLevel.Debug);

            AttachLoggerToUnityConsole(Unity3DDefaultLoggerName);

        }

        protected override ILogger CreateLoggerInstance(string name, ELogLevel logLevel)
        {
            return new UnityLogger(name, logLevel);
        }

    }

}
