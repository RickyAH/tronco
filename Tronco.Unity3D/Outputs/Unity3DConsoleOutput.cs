﻿using System.Collections.Generic;
using Tronco.Core;
using Tronco.Outputs;

namespace Tronco.Unity3D.Outputs
{
    public class Unity3DConsoleOutput : ILogOutput
    {
        public delegate void UnityDevConsoleLoggerDlg(string msg);

        #region ILogOutput implementation

        public void OutputEntries(IEnumerable<FormattedLogEntry> logEntries)
        {
            foreach(var logEntry in logEntries)
            {
                SelectUnityDevConsoleLoggerFunc(logEntry.Entry)(logEntry.FormattedEntry);
            }

        }

        #endregion

        UnityDevConsoleLoggerDlg SelectUnityDevConsoleLoggerFunc(LogEntry entry)
        {
            switch(entry.LogLevel)
            {
                case ELogLevel.Error: return UnityEngine.Debug.LogError;
                case ELogLevel.Fatal: return UnityEngine.Debug.LogError;
                case ELogLevel.Warning: return UnityEngine.Debug.LogWarning;
                default: return UnityEngine.Debug.Log;
            }
        }
    }
}
