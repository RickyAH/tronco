using NUnit.Framework;
using Tronco.Core;
using Tronco.Formatters;
using Tronco.Outputs.UDPOutputs;

namespace Tronco.Tests.Unit.Outputs
{
    [TestFixture]
    public class UDPOutputTests
    {
        [Test]
        public void SendToAny()
        {
            var formatter = new DefaultLogEntryFormatter();

            var logEntry1 = formatter.FormatLogEntry(new LogEntry(ELogLevel.Debug, "Test 10.0.1.255"));
            var logEntry2 = formatter.FormatLogEntry(new LogEntry(ELogLevel.Debug, "Test 255.255.255.255"));
            var logEntry3 = formatter.FormatLogEntry(new LogEntry(ELogLevel.Debug, "Test 0.0.0.0"));
            var logEntry4 = formatter.FormatLogEntry(new LogEntry(ELogLevel.Debug, "Test 10.0.1.105"));

            new UdpOutput(new UdpOutputConfiguration("10.0.1.255", 11000)).OutputEntries(new [] {logEntry1});
            new UdpOutput(new UdpOutputConfiguration("255.255.255.255", 11000)).OutputEntries(new [] {logEntry2});
            new UdpOutput(new UdpOutputConfiguration("0.0.0.0", 11000)).OutputEntries(new [] {logEntry3});

            new UdpOutput(new UdpOutputConfiguration("10.0.1.105", 11000)).OutputEntries(new [] {logEntry4});
        }

    }

}