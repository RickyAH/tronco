using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using Tronco.Core;
using Tronco.Outputs.FileOutputs;

namespace Tronco.Tests.Unit.Outputs
{
    [TestFixture]
    public class FileOutputTests
    {
        string testLogFilePath;

        [SetUp]
        public void Setup()
        {
            testLogFilePath = CombinePath(".", "test", "log.txt");
            File.Create(testLogFilePath).Close();
        }

        [Test]
        public void FileIsCreated()
        {
            using(var file = new FileLogOutput(testLogFilePath))
            {
                Assert.That(File.Exists(testLogFilePath));
            }

            Assert.That(File.ReadAllText(testLogFilePath), Is.StringContaining("Session started on"));
        }

        [Test]
        public void WritingEntriesWorks()
        {
            var fileOutput = new FileLogOutput(testLogFilePath);

            WriteEntriesToLogOutput(fileOutput, 1 ,"hello", "world");

            var result = SplitLogStr(File.ReadAllText(testLogFilePath));

            // by default we add a line logging the time & date when this
            // session started
            Assert.That(result, Has.Length.EqualTo(3));
            Assert.That(result[1], Is.EqualTo("hello"));
            Assert.That(result[2], Is.EqualTo("world"));
        }

        [Test]
        public void AppendingEntriesToFile()
        {
            var config = new FileLogOutputConfig { OverwriteFileOnNewSession = false};
            var fileOutput = new FileLogOutput(testLogFilePath, config);


            WriteEntriesToLogOutput(fileOutput, 2 ,"hello", "world");

            var result = SplitLogStr(File.ReadAllText(testLogFilePath));
            // We start t
            Assert.That(result, Has.Length.EqualTo(4));
            Assert.That(result[0], Is.EqualTo("hello"));
            Assert.That(result[1], Is.EqualTo("hello"));
            Assert.That(result[2], Is.EqualTo("world"));
            Assert.That(result[3], Is.EqualTo("world"));
        }

        [Test]
        public void CreatingNewFileForEachSession()
        {
            var config = new FileLogOutputConfig { OverwriteFileOnNewSession = true, AppendDateAndTimeOnNewSession = false};
            var fileOutput = new FileLogOutput(testLogFilePath, config);

            WriteEntriesToLogOutput(fileOutput, 5 ,"hello", "world");

            var result = SplitLogStr(File.ReadAllText(testLogFilePath));

            Assert.That(result, Has.Length.EqualTo(2));
            Assert.That(result[0], Is.EqualTo("hello"));
            Assert.That(result[1], Is.EqualTo("world"));
        }

        void WriteEntriesToLogOutput(FileLogOutput fileOutput, int numTestSessions, params string[] entries)
        {
            for(int idx = 0; idx < numTestSessions; idx++)
            {
                fileOutput.Open();
                fileOutput.OutputEntries(entries.Select(e => new FormattedLogEntry{FormattedEntry = e}));
                fileOutput.Close();
            }
        }

        string[] SplitLogStr(string str)
        {
            return str.Split( new [] {Environment.NewLine},  StringSplitOptions.RemoveEmptyEntries);
        }

        string CombinePath(params string[] subPaths)
        {
            return string.Join(Path.DirectorySeparatorChar.ToString(), subPaths);
        }
    }
    
}