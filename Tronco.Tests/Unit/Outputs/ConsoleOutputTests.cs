using NUnit.Framework;
using Tronco.Core;
using Tronco.Outputs.ConsoleOutputs;

namespace Tronco.Tests.Unit.Outputs
{
    [TestFixture]
    public class ConsoleOutputTests
    {
        [Test]
        public void CreateOutputLogger()
        {
            var consoleOutput = new ConsoleLogOutput();

            var entry = new FormattedLogEntry{FormattedEntry = "Hello world!"};

            consoleOutput.OutputEntries(new [] {entry} );

            Assert.That(true, "Check that 'Hello world!' was printed to the console");
        }
    }
}