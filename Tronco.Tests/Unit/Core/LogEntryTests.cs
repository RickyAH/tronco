using NUnit.Framework;
using Tronco.Core;

namespace Tronco.Tests.Unit.Core
{

    [TestFixture]
    public class LogEntryTests
    {
        [Test]
        public void GathersAdditionalDataOnInstantiation()
        {
            var entry = new LogEntry(ELogLevel.Info, string.Empty);

            Assert.That(entry.Timestamp, Is.Not.Null);
            Assert.That(entry.StackTraceSnapshot, Is.Not.Null);
        }

        [Test]
        public void CreatingEmptyLogEntriesGivesAMessage()
        {
            var entry = new LogEntry(ELogLevel.Info, string.Empty);

            Assert.That(entry.Message, Is.Not.Null);
            Assert.That(entry.Message, Is.Not.Empty);
            System.Console.WriteLine(entry.Message);
        }

        [Test]
        public void CreatingBadFormattedMessageShowsError()
        {
            var entry = new LogEntry(ELogLevel.Info, "{0} - {2}", 1);

            Assert.That(entry.Message, Is.Not.Null);
            Assert.That(entry.Message, Is.Not.Empty);
            System.Console.WriteLine(entry.Message);
        }

        [Test]
        public void WhenUsingDelegatesTheMessageIsNotCreatedUntilAccessed()
        {
            bool flag = false;
            var entry = new LogEntry(ELogLevel.Info, () => { flag = true; return "Testing message log with delegates"; });

            Assert.That(flag, Is.False);
            
            System.Console.WriteLine(entry.Message); 

            Assert.That(flag, Is.True);
        }

        [Test]
        public void BewareOfScopesWhenUsingDelegates()
        {
            string mutableStr = "12345";
            var entry = new LogEntry(ELogLevel.Info, () => "Testing message log with delegates:  " + mutableStr);
            
            mutableStr = "abcde";
            System.Console.WriteLine(entry.Message);

            Assert.That(entry.Message, Is.StringContaining("abcde"));
        }

       
    }
}