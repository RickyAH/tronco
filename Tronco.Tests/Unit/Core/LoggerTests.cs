using NUnit.Framework;
using Tronco.Core;

namespace Tronco.Tests.Unit.Core
{
    [TestFixture]
    public class LoggerTests
    {

        [Test]
        public void RaisesEventOnLogCreated()
        {
            var flag = false;
            var logger = new Logger("Test", ELogLevel.Fatal);
            logger.LogEntryCreated += entry => flag = true;

            logger.Fatal("This should raise the event");
            Assert.That(flag, Is.True);
        }

        [Test]
        public void DoesNotRaisesEventOnLogCreatedIfFiltered()
        {
            var flag = false;
            var logger = new Logger("Test", ELogLevel.Fatal);
            logger.LogEntryCreated += entry => flag = true;

            logger.Info("This should not raise the event");
            Assert.That(flag, Is.False);
        }

        [Test]
        public void measure_code_to_execute()
        {
            var waitTimeMs = 150;
            var flag = false;
            var logger = new Logger("Test", ELogLevel.Debug);

            logger.LogEntryCreated += entry =>
            {
                flag = true;
                Assert.That(entry is MeasurementLogEntry);
                Assert.That(entry.Message, Is.EqualTo("test"));
                Assert.That(entry.LogLevel == ELogLevel.Debug);
                Assert.That((entry as MeasurementLogEntry).ElapsedTime.Milliseconds > waitTimeMs);

            };

            logger.StartMeasuring("test");
            Assert.That(flag, Is.False);

            System.Threading.Thread.Sleep(2*waitTimeMs);

            logger.StopMeasuring();
            Assert.That(flag, Is.True);
        }

        [Test]
        public void measure_multiple_codes()
        {
            var waitTimeMs = 150;
            var flag = 0;
            var logger = new Logger("Test", ELogLevel.Debug);

            logger.LogEntryCreated += entry =>
            {
                flag++;
                Assert.That(entry is MeasurementLogEntry);
                Assert.That(entry.LogLevel == ELogLevel.Debug);
                Assert.That((entry as MeasurementLogEntry).ElapsedTime.Milliseconds > waitTimeMs);

            };

            logger.StartMeasuring("test");


            System.Threading.Thread.Sleep(waitTimeMs);
            logger.StartMeasuring("test2");
            System.Threading.Thread.Sleep(waitTimeMs);

            logger.StopMeasuring();

            logger.StopMeasuring();

            Assert.That(flag, Is.EqualTo(2));

            logger.StopMeasuring();
            logger.StopMeasuring();
            Assert.That(flag, Is.EqualTo(2));
        }


        [Test]
        public void measure_block()
        {
            var waitTimeMs = 150;
            var flag = false;
            var logger = new Logger("Test", ELogLevel.Debug);

            logger.LogEntryCreated += entry =>
            {
                flag = true;
                Assert.That(entry is MeasurementLogEntry);
                Assert.That(entry.Message, Is.EqualTo("test"));
                Assert.That(entry.LogLevel == ELogLevel.Debug);
                Assert.That((entry as MeasurementLogEntry).ElapsedTime.Milliseconds > waitTimeMs);

            };

            logger.MeasureBlock("test", () => {
                System.Threading.Thread.Sleep(2*waitTimeMs);
            });

            Assert.That(flag, Is.True);
        }
    }
}

