using NUnit.Framework;
using Tronco.Core;

namespace Tronco.Tests.Unit
{
    public class MyRepository : BaseLogRepositorySingleton<MyRepository>
    {
        public static ILogger UI {get { return  Instance.CreateLogger("UI", ELogLevel.Debug);}}
    }

    public class MyRepository2 : BaseLogRepositorySingleton<MyRepository2>
    {
        public static ILogger UI {get { return  Instance.CreateLogger("UI", ELogLevel.Info);}}
    }

    [TestFixture]
    public class LogRepositoryTests
    {

        [Test]
        public void AllowMultipleRepositoryInstances()
        {
            Assert.That(MyRepository2.Instance, Is.Not.EqualTo(MyRepository.Instance));

            Assert.That(MyRepository.UI, Is.Not.EqualTo(MyRepository2.UI));
        }

        [Test]
        public void CanCreateLoggers()
        {
            var logger1 = MyRepository.Instance.CreateLogger("test1", ELogLevel.Info);
            Assert.That(logger1, Is.Not.Null);

            Assert.That(MyRepository.Instance.HasLogger("test1"), Is.True);

        
            MyRepository.Instance.CreateLogger("test2", ELogLevel.Info);
            Assert.That(MyRepository.Instance.HasLogger("test2"), Is.True);
            MyRepository.Instance.DeleteLogger("test2");
            Assert.That(MyRepository.Instance.HasLogger("test2"), Is.False);

            var logger2 = MyRepository.Instance.CreateLogger("test1", ELogLevel.Info);
            Assert.That(logger1, Is.EqualTo(logger2));
            Assert.That(MyRepository.Instance.HasLogger("test1"), Is.True);

            MyRepository.Instance.DeleteLogger("test1");
            logger1 = MyRepository.Instance.CreateLogger("test1", ELogLevel.Info);

            Assert.That(logger1, Is.Not.EqualTo(logger2));
        }

        [Test]
        public void CanCreateTargets()
        {
            var target1 = new Tronco.Targets.LogTarget("test1");
            MyRepository.Instance.AddTarget(target1);
            Assert.That(MyRepository.Instance.HasTarget("test1"), Is.True);

            MyRepository.Instance.CreateLogger("log1", ELogLevel.Debug);
            var result = MyRepository.Instance.AttachLoggerToTarget("log1", "test1");
            Assert.That(result, Is.True);
            Assert.That(target1.Loggers["log1"], Is.Not.Null);

            MyRepository.Instance.RemoveTarget("test1");
            Assert.That(MyRepository.Instance.HasTarget("test1"), Is.False);
            Assert.That(target1.Loggers["log1"], Is.Null);
            Assert.That(target1.Loggers, Is.Empty);

            MyRepository.Instance.DeleteLogger("test1");

        }

        [Test]
        public void NotifyChangesInLoggers()
        {
            var logName = string.Empty;
            
            MyRepository.Instance.LogAdded += logger => logName = logger.Name;
            MyRepository.Instance.LogRemoved += logger => logName = logger.Name;

            MyRepository.Instance.CreateLogger("test", ELogLevel.Info);

            Assert.That(logName, Is.EqualTo("test"));
            MyRepository.Instance.CreateLogger("test2", ELogLevel.Info);
            Assert.That(logName, Is.EqualTo("test2"));

            MyRepository.Instance.DeleteLogger("test");
            Assert.That(logName, Is.EqualTo("test"));
            
            MyRepository.Instance.DeleteLogger("test3");
            Assert.That(logName, Is.EqualTo("test"));
        }
    }
}