using System;
using NUnit.Framework;
using Tronco.Core;
using Tronco.Formatters;

namespace Tronco.Tests.Unit.Formatters
{
    public class Log4NetFormatterTests
    {
        [Test]
        public void RecognicesKeyMessage()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new SpecialLogEntryFormatter("Hello $$(message)");

            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo("Hello $World"));
        }

        [Test]
        public void IgnoresUnrecognicedKey()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new SpecialLogEntryFormatter("Hello $(mykey)");

            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo("Hello $(mykey)"));
        }

        [Test]
        public void RecognicesKeyNewLine()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new SpecialLogEntryFormatter("Hello $(newline) $(message)");

            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo("Hello " + Environment.NewLine + " World"));
        }

        [Test]
        public void RecognicesKeyTime()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new SpecialLogEntryFormatter("$(time_now)");
            DateTime d;
            Assert.That(DateTime.TryParse(formatter.FormatLogEntry(entry).FormattedEntry, out d), Is.True);
        }

        [Test]
        public void RecognicesKeyTimeNow()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new SpecialLogEntryFormatter("$(time_now)");
            DateTime d;
            Assert.That(DateTime.TryParse(formatter.FormatLogEntry(entry).FormattedEntry, out d), Is.True);
        }


        [Test]
        public void RecognicesFormatForTimeKeywords()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new SpecialLogEntryFormatter("$(time_now:YYYY)");

            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo(DateTime.Now.ToString("YYYY")));
        }

    }
}