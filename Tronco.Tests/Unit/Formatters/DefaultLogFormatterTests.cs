using System;
using NUnit.Framework;
using Tronco.Core;
using Tronco.Formatters;

namespace Tronco.Tests.Unit.Formatters
{
    [TestFixture]
    public class DefaultLogFormatterTests
    {
        [Test]
        public void RecognicesKeyMessage()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("[{level}] Hello {message}");

            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo("[Info] Hello World"));
        }

        [Test]
        public void IgnoresUnrecognicedKey()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("Hello {mykey}");

            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo("Hello {mykey}"));
        }

        [Test]
        public void RecognicesKeyNewLine()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("Hello {newline} {message}");

            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo("Hello " + Environment.NewLine + " World"));
        }

        [Test]
        public void RecognicesKeyTimestamp()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("{timestamp}");
            DateTime d;
            Assert.That(DateTime.TryParse(formatter.FormatLogEntry(entry).FormattedEntry, out d), Is.True);
        }

        [Test]
        public void RecognicesKeyTimeNow()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("{time_now}");
            DateTime d;
            Assert.That(DateTime.TryParse(formatter.FormatLogEntry(entry).FormattedEntry, out d), Is.True);
        }

        [Test]
        public void RecognicesFormatForTimeKeywords()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("{time_now:YYYY}");
   
            Assert.That(formatter.FormatLogEntry(entry).FormattedEntry, Is.EqualTo(DateTime.Now.ToString("YYYY")));
        }


        [Test]
        public void DoesNotThrowExceptionIfFails()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("Hello {message} {filename} {linenumber");

            formatter.FormatLogEntry(entry);
        }
        [Test]
        public void RecognicesDebugInfoKeys()
        {
            LogEntry entry = new LogEntry(ELogLevel.Info, "World");
            var formatter = new DefaultLogEntryFormatter("{method} {filename}:{linenumber}");

            Console.WriteLine(formatter.FormatLogEntry(entry).FormattedEntry);
        }

    }
}