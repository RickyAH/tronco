using NUnit.Framework;
using Tronco.Core;
using Tronco.Filters;

namespace Tronco.Tests.Unit.Filters
{
    [TestFixture]
    public class MinimumLogLevelFilterTest
    {
        [Test]
        public void CheckFilterByLogLevel()
        {
            var logEntry1 = new LogEntry(ELogLevel.Debug, "le1");
            var logEntry2 = new LogEntry(ELogLevel.Info, "le2");
            var logEntry3 = new LogEntry(ELogLevel.Warning, "le3");
            var logEntry4 = new LogEntry(ELogLevel.Error, "le4");
            var logEntry5 = new LogEntry(ELogLevel.Fatal, "le5");

            var logLevelFilter = new MinimumLogLevelFilter(ELogLevel.Error);


            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry1), Is.True);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry2), Is.True);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry3), Is.True);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry4), Is.False);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry5), Is.False);

            
            logLevelFilter.MinimumLogLevel = ELogLevel.Warning;

            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry1), Is.True);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry2), Is.True);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry3), Is.False);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry4), Is.False);
            Assert.That(logLevelFilter.ShouldDiscardLogEntry(logEntry5), Is.False);
        }
    }
}