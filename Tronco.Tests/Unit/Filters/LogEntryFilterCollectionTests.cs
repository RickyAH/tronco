using System.Linq;
using NUnit.Framework;
using Tronco.Core;
using Tronco.Filters;

namespace Tronco.Tests.Unit.Filters
{
    [TestFixture]
    public class LogEntryFilterCollectionTests
    {
        [Test]
        public void CheckBasicLogEntryFiltering()
        {
            var logEntry1 = new LogEntry(ELogLevel.Error, "le1");
            var logEntry2 = new LogEntry(ELogLevel.Info, "le2");
            var logEntry3 = new LogEntry(ELogLevel.Fatal, "le3");
            var filterer = new LogEntryFilterCollection()
                .AddRejectIfFilter(logEntry => logEntry.LogLevel < ELogLevel.Error );

            var result = filterer.FilterEntries(new[] {
                                                        logEntry1,
                                                        logEntry2, 
                                                        logEntry3
                                                    });
            
            Assert.That(result.Contains(logEntry1), Is.True);
            Assert.That(result.Contains(logEntry2), Is.False);
            Assert.That(result.Contains(logEntry3), Is.True);
        }

        [Test]
        public void CheckEmptyFilters()
        {
            var logEntry1 = new LogEntry(ELogLevel.Error, "le1");
            var logEntry2 = new LogEntry(ELogLevel.Info, "le2");
            var logEntry3 = new LogEntry(ELogLevel.Fatal, "le3");
            var filterer = new LogEntryFilterCollection();

            var result = filterer.FilterEntries(new[] { logEntry1, logEntry2, logEntry3 });

            Assert.That(result.Contains(logEntry1), Is.True);
            Assert.That(result.Contains(logEntry2), Is.True);
            Assert.That(result.Contains(logEntry3), Is.True);
        }


        [Test]
        public void CheckFiltersNotMatching()
        {
            var logEntry1 = new LogEntry(ELogLevel.Error, "le1");
            var logEntry2 = new LogEntry(ELogLevel.Error, "le2");
            var logEntry3 = new LogEntry(ELogLevel.Fatal, "le3");
            var filterer = new LogEntryFilterCollection()
                .AddRejectIfFilter(logEntry => logEntry.LogLevel < ELogLevel.Error );

            var result = filterer.FilterEntries(new[] { logEntry1, logEntry2, logEntry3 });

            Assert.That(result.Contains(logEntry1), Is.True);
            Assert.That(result.Contains(logEntry2), Is.True);
            Assert.That(result.Contains(logEntry3), Is.True);
        }
    }
}